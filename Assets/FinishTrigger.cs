using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishTrigger : MonoBehaviour
{
    [SerializeField] private Canvas finishUI;
    private Transform otherPlayer;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Swag");
        otherPlayer = other.transform.parent;
        finishUI.enabled = true;
    }

    private void Update()
    {
        if(otherPlayer != null)
            transform.parent.position =
            Vector3.MoveTowards(transform.parent.position, otherPlayer.transform.position, 0.1f);
    }
}
