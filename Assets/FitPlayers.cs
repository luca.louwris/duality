using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Unity.Mathematics;
using UnityEngine;

[ExecuteInEditMode]
public class FitPlayers : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera cam;
    [SerializeField] private Camera textureCam;
    [SerializeField] List<Transform> dots = new List<Transform>();
    [SerializeField] private float min; 
    public bool Locked;

    public MeshRenderer border;

    [ExecuteInEditMode]
    private void Update()
    {
        float size = math.clamp(Vector3.Distance(dots[0].position, dots[1].position) / 1.3f, min, math.INFINITY);
        cam.m_Lens.OrthographicSize = size;
        textureCam.orthographicSize = size;
        border.enabled = false;
        if (Locked)
        {            
            border.enabled = true;
            return;
        }
        
        Vector3 total = new Vector3();
        foreach (var p in dots)
        {
            var pos = p.position;
            total.x += pos.x;
            total.y += pos.y;
        }
        transform.LookAt(dots[0]);
        transform.position = total / dots.Count;
    }
}
