using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    [SerializeField] private GameObject[] players;
    private Canvas finishUI;

    private void OnTriggerEnter2D(Collider2D other)
    {
        FindObjectOfType<FitPlayers>().gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }
}
