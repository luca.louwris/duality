using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private FitPlayers barrier;
    public float moveSpeed;
    [SerializeField] private Rigidbody2D body;
    
    private Vector2 m_Move;
    [SerializeField] private bool player1;
    [SerializeField] private Vector2 jumpForce;
    
    [SerializeField] private bool grounded;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private Vector3 normalLine;
    [SerializeField] private float maxSpeed;
    private SimpleControls controls;

    private float JumpTimeOut = 0.1f;
    private float TimeSinceJump;
    private void Start()
    {
        controls = new SimpleControls();
        if(player1)
        {
            controls.Player1.move.performed += OnMove;
            controls.Player1.move.canceled += OnMove;
            controls.Player1.Lock.performed += OnLock;
        }else if (!player1)
        {
            controls.Player2.move.performed += OnMove;
            controls.Player2.move.canceled += OnMove;
            controls.Player2.Lock.performed += OnLock;
        }
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        m_Move = context.ReadValue<Vector2>();
    }

    public void OnLock(InputAction.CallbackContext context)
    {
        barrier.Locked = !barrier.Locked;
    }

    

    public void Update()
    {
        // Update orientation first, then move. Otherwise move orientation will lag
        // behind by one frame.
        grounded = IsGrounded();
        TimeSinceJump += Time.deltaTime;
        Jump(m_Move.y);
        Move(m_Move.x);
    }

    private bool IsGrounded()
    {
        Vector2 castPos = new Vector2(transform.position.x, transform.position.y);
        RaycastHit2D[] hits = Physics2D.CircleCastAll(castPos, 0.48f, Vector2.down, 0.55f, layerMask);
        normalLine = Vector2.zero;
        foreach (var VARIABLE in hits)
        {
            normalLine += (Vector3) VARIABLE.normal;
        }
        normalLine.Normalize();
        if(normalLine == Vector3.zero)
            return false;
        return true;
    }

    private void Jump(float mJump)
    {
        if (!grounded) return;
        if (TimeSinceJump < JumpTimeOut) return;
        if (mJump < 0.01 && mJump > -0.01)
            return;
        if (mJump > 0.01)
        {
            m_Move.y = 0;
            TimeSinceJump = 0;
            body.AddRelativeForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
    }

    private void Move(float direction)
    {
        if (direction < 0.01 && direction > -0.01)
            return;
        var scaledMoveSpeed = moveSpeed;
        // For simplicity's sake, we just keep movement in a single plane here. Rotate
        // direction according to world Y rotation of player.
        Vector3 moveDir = new Vector3(direction, 0, 0) * scaledMoveSpeed;
        if(normalLine != Vector3.up || normalLine != Vector3.right || normalLine != Vector3.left)
            moveDir = Vector3.ProjectOnPlane(moveDir, normalLine);
        
        body.AddRelativeForce(new Vector2(moveDir.x, moveDir.y));
        if (body.velocity.x > maxSpeed)
            body.velocity = new Vector2(maxSpeed, body.velocity.y);
        if (body.velocity.x < -maxSpeed)
            body.velocity = new Vector2(-maxSpeed, body.velocity.y);
    }
}
