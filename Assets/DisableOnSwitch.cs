using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnSwitch : MonoBehaviour
{
    [SerializeField] private bool inDeath;
    [SerializeField] private GameObject door;
    [SerializeField] private SpriteRenderer ai;
    [SerializeField] private GameObject DoorParticles;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Death"))
        {
            inDeath = true;
            TurnColor(Color.red);
            Invoke(nameof(TurnColor),0.5f);
        }
        Debug.Log(other);
        if (other.gameObject.CompareTag("Player") && !inDeath)
        {
            TurnColor(Color.green);
            door.SetActive(false);
            Invoke(nameof(TurnColor),0.5f);
        }
    }

    private void TurnColor(Color color)
    {
        if (color == null)
            ai.color = Color.black;
        else
            ai.color = color;

    }

    private void OnDisable()
    {
        Instantiate(DoorParticles, transform.position, Quaternion.identity);
    }
}
