using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisable : MonoBehaviour
{
    public Canvas Canvas;
    public void SwitchCanvas()
    {
        Canvas.enabled = !Canvas.isActiveAndEnabled;
    }
}
